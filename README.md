## Specification

This project developed using NodeJS with ExpressJS
Node version: v16.20.0
NPM version: 8.19.4

## Installation

- Create database and set database configuration in config.js
- Import database.sql into database
- Make sure you already install nodejs in your machine
- Copy files into your development folder and open terminal in root folder
- Type in terminal `npm i` to install all requirement packages
- Run the code by typing `node index`

## NOTE

- The condition is `send a happy birthday message to users on their birthday at exactly 9 am on their local time` but there is also condition like this `The system needs to be able to recover and send all unsent messages if the service was down for a period of time (say a day).`. So I try to still send the message per hour if the email is failed.

- I use soft delete to delete data so you will get error when deleting and insert with same email address. please run delete query manually by selected `deleted_at IS NOT NULL`.