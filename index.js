const cron = require('node-cron');
const express = require('express');
const axios = require('axios');
const moment = require('moment-timezone');

const app = express();

const user = require('./services/user');
const userRouter = require("./routes/user");

// Check user data that has birthday today
const getUserData = async () => {
  try {
    const result = await user.getAll();
    const users = result.data;

    if (users.length > 0) {
      let promises = [];
      for (i = 0; i < users.length; i++) {
        if (moment().tz(users[i].country_timezone).hour() == 9) {
          promises.push(
            axios.post("https://email-service.digitalenvision.com.au/send-email", {
              "email": users[i].email,
              "message": `Hey, ${users[i].first_name} ${users[i].last_name} it's your birthday`
            }, {
              headers: {
                "Content-Type": "application/json",
              },
            })
              .then(({ data }) => {
                // update database if email success
                console.log("### update latest_send_at in table user if email success");
                console.log(data);
              })
              .catch((error) => {
                console.error({
                  "status": "failed",
                  "response_status": error?.response?.status,
                  "message": error?.message
                });
              })
          )
        }
      }

      Promise.all(promises)
        .then(() => console.log('all done'))
        .catch((error) => {
          console.error(error);
        });

    } else {
      console.log("No birthday today");
    }

    console.log(users);
  } catch (err) {
    console.error(`Error while getting users `, err.message);
  }
}

cron.schedule('*/30 * * * *', () => {
  getUserData();
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => { res.json({ message: "ok" }) });
app.use("/user", userRouter);

// Middleware error handling
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  console.error(err.message, err.stack);
  res.status(statusCode).json({ message: err.message });
  return;
});

app.listen(3000);