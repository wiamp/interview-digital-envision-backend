const express = require('express');
const router = express.Router();
const user = require('../services/user');

// GET user
router.get('/', async function (req, res, next) {
  try {
    res.json(await user.getAll());
  } catch (err) {
    console.error(`Error while getting users `, err.message);
    next(err);
  }
});

// POST user
router.post('/', async function (req, res, next) {
  try {
    res.json(await user.create(req.body));
  } catch (err) {
    console.error(`Error while creating user`, err.message);
    next(err);
  }
});

router.delete('/:id', async function(req, res, next) {
  try {
    res.json(await user.softDelete(req.params.id));
  } catch (err) {
    console.error(`Error while deleting user`, err.message);
    next(err);
  }
});

module.exports = router;