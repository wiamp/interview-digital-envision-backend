const db = require('./db');
const helper = require('../helper');
const config = require('../config');

async function getAll() {
  const rows = await db.query(`
    SELECT 
      u.id, 
      u.first_name, 
      u.last_name, 
      u.email, 
      u.country_id, 
      c.name country_name,
      c.timezone country_timezone,
      c.utc country_utc
    FROM user u
    JOIN country c 
      ON c.id = u.country_id
      AND c.deleted_at IS NULL
    WHERE u.deleted_at IS NULL
      AND u.latest_email_at IS NULL
      AND DATE_FORMAT(u.birthday_date, "%m%d") IN (DATE_FORMAT(NOW() - INTERVAL 1 DAY, "%m%d"), DATE_FORMAT(NOW(), "%m%d"), DATE_FORMAT(NOW() + INTERVAL 1 DAY, "%m%d"))
  `);

  const data = helper.emptyOrRows(rows);

  return {
    data
  }
}

async function create(data) {
  const result = await db.query(`
    INSERT INTO user (
      first_name, 
      last_name, 
      email, 
      birthday_date, 
      country_id
    ) VALUES (?, ?, ?, ?, ?)
  `, [
    data.first_name,
    data.last_name,
    data.email,
    data.birthday_date,
    data.country_id
  ]);

  let message = 'Error in creating user data';

  if (result.affectedRows) {
    message = 'User data created successfully';
  }

  return { message };
}

async function softDelete(id) {
  const result = await db.query(`
    UPDATE user
    SET deleted_at = NOW()
    WHERE id = ?
  `, [id]);

  let message = 'Error in deleting user data';

  if (result.affectedRows) {
    message = 'User data deleted successfully';
  }

  return { message };
}

module.exports = {
  getAll,
  create,
  softDelete
}